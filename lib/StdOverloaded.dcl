definition module StdOverloaded

/**
 * A number of common overloaded functions.
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 2.0
//	Copyright 1998 University of Nijmegen
// ****************************************************************************************

//* Add `arg1` to `arg2`.
class  (+)  infixl 6	a	:: !a	!a	->	a

//* Subtract `arg2` from `arg1`.
class  (-)  infixl 6	a	:: !a	!a	->	a

//* Zero (unit element for addition).
class  zero 			a	:: a


//* Multiply `arg1` with `arg2`.
class  (*)  infixl 7	a	:: !a	!a	->	a

//* Divide `arg1` by `arg2`.
class  (/)  infixl 7	a	:: !a	!a	->	a

//* One (unit element for multiplication).
class  one 			a	:: a


//* True if `arg1` is equal to `arg2`.
class  (==) infix  4	a	:: !a	!a	->	Bool

//* True if `arg1` is less than `arg2`.
class  (<)  infix  4	a	:: !a	!a	->	Bool

//* True if `arg1` is an even number.
class  isEven a :: !a -> Bool;

//* True if `arg1` is an odd  number.
class  isOdd  a :: !a -> Bool;


/**
 * Number of elements in `arg`.
 * Used for list-like structures. It should have O(n) complexity.
 */
class  length m	:: !(m a) -> Int

//* Slice a part from `arg1`.
class  (%)  infixl 9	a	:: !a !(!Int,!Int)	-> a


//* Append the arguments.
class  (+++) infixr 5	a 	:: !a	!a	-> a


//* `arg1` to the power of `arg2`.
class  (^)  infixr 8	a	:: !a	!a 	->	a

//* Absolute value.
class  abs  			a	:: !a		->	a

/**
 * The sign of a thing.
 * @result `1` for a positive value; `-1` for a negative value; `0` for zero
 */
class  sign 			a	:: !a 		->	Int

//* Negate a thing.
class  ~				a	:: !a 		->	a


//* `arg1` modulo `arg2`.
class  (mod) infix 7 a :: !a !a -> a

//* The remainder after division of `arg1 / arg2`.
class  (rem)	infix 7	a :: !a !a -> a

//* Greatest common divisor.
class  gcd a :: !a !a -> a

//* Least common multiple.
class  lcm a :: !a !a -> a


//* Convert into {{`Int`}}.
class  toInt			a	:: !a		->	Int

//* Convert into {{`Char`}}.
class  toChar		a	:: !a		->	Char

//* Convert into {{`Bool`}}.
class  toBool		a	:: !a		->	Bool

//* Convert into {{`Real`}}.
class  toReal		a	:: !a		->	Real

//* Convert into {{`String`}}.
class  toString		a	:: !a		->	{#Char}


//* Convert from {{`Int`}}.
class  fromInt		a	:: !Int			-> a

//* Convert from {{`Char`}}.
class  fromChar		a	:: !Char		-> a

//* Convert from {{`Bool`}}.
class  fromBool		a	:: !Bool		-> a

//* Convert from {{`Real`}}.
class  fromReal		a	:: !Real		-> a

//* Convert from {{`String`}}.
class  fromString	a	:: !{#Char}		-> a


//* Logarithm base e.
class  ln			a	:: !a 		->	a

//* Logarithm base 10.
class  log10			a	:: !a 		->	a

//* e to to the power of `arg`.
class  exp			a	:: !a 		->	a

//* Square root.
class  sqrt			a	:: !a 		->	a


//	Trigonometrical Functions:

//* Sine.
class  sin			a	:: !a		->	a

//* Cosine.
class  cos			a	:: !a		->	a

//* Tangent.
class  tan			a	:: !a		->	a

//* Arc Sine.
class  asin			a	:: !a		->	a

//* Arc Cosine.
class  acos			a	:: !a		->	a

//* Arc Tangent.
class  atan			a	:: !a		->	a

//* Hyperbolic Sine.
class  sinh			a	:: !a		->	a

//* Hyperbolic Cosine.
class  cosh			a	:: !a		->	a

//* Hyperbolic Tangent.
class  tanh			a	:: !a		->	a

//* Arc Hyperbolic Sine.
class  asinh			a	:: !a		->	a

//* Arc Hyperbolic Cosine.
class  acosh			a	:: !a		->	a

//* Arc Hyperbolic Tangent.
class  atanh			a	:: !a		->	a
