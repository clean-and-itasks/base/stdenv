definition module StdMisc

/**
 * Miscellaneous functions.
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 3.0
//	Copyright 2019 University of Nijmegen
// ****************************************************************************************

/**
 * Print a message and abort the program.
 *
 * @param The message to print
 * @result There is no result; the program will terminate
 */
abort		:: !{#Char} -> .a				:== code { .d 1 0 ; jsr print_string_ ; .o 0 0 ; halt }

/**
 * The undefined value.
 *
 * @result An attempt to evaluate the result will yield a runtime error.
 */
undef		:: .a
