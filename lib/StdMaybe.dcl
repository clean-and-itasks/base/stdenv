definition module StdMaybe

//	********************************************************************************
//	Clean StdLib library module, version 3.0
//	********************************************************************************

from StdOverloaded import class ==(..);
import _SystemStrictMaybes

/**
 * This type synonym exists for backwards compatibility. New code should use
 * one of the builtin types `?`, `?^`, or `?#`. Old code should be updated.
 */
:: Maybe x :== ?^ x

//* Legacy. Use one of the builtin `?Just` constructors instead.
Just x :== ?^Just x
//* Legacy. Use one of the builtin `?None` constructors instead.
Nothing :== ?^None

//* `case @1 of ({{Just}} _) -> True; _ -> False`
isJust m 	:== m =: ?|Just _

//* `\({{Just}} x) -> x`
fromJust m	:== let (?|Just x) = m in x

//* Legacy. Use `{{isNone}}` instead.
isNothing m	:== m =: ?|None

//* `{{not}} {{o}} {{isJust}}`
isNone m	:== m =: ?|None

//* {{`isJust`}} for a possibly unique value.
isJustU :: !u:(m v:a) -> .(!Bool, !u:(m v:a)) | Maybe m a, [u <= v]
	special m= ?; m= ?^
//* Legacy. Use `{{isNoneU}}` instead.
isNothingU :: !u:(m v:a) -> .(!Bool, !u:(m v:a)) | Maybe m a, [u <= v]
	special m= ?; m= ?^
//* {{`isNone`}} for a possibly unique value.
isNoneU :: !u:(m v:a) -> .(!Bool, !u:(m v:a)) | Maybe m a, [u <= v]
	special m= ?; m= ?^

//* Apply a function on the value boxed in a {{`Just`}}, or pass on a {{`None`}}.
mapMaybe :: .(v:a -> w:b) !u:(m v:a) -> u:(m w:b) | Maybe m a & Maybe m b, [u<=v, u<=w]
	special m= ?; m= ?^

instance == (?x) | == x
instance == (?^x) | == x
instance == (?#x) | UMaybe x & == x

/**
 * @result A list with no or one element.
 */
maybeToList :: !u:(m v:a) -> .[v:a] | Maybe m a, [u<=v]
	special m= ?; m= ?^

/**
 * @result {{`?|Just`}} the head of the list or {{`?|None`}} if it is empty.
 */
listToMaybe :: !u:[v:a] -> w:(m v:a) | Maybe m a, [w u <= v]
	special m= ?; m= ?^

//* `catMaybes ms = [m \\ {{?|Just}} m <- ms]`
catMaybes :: ![u:(m v:a)] -> .[v:a] | Maybe m a, [u<=v]
	special m= ?; m= ?^
