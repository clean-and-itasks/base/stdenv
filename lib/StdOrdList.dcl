definition module StdOrdList

/**
 * Functions to work with lists of elements for which an ordering exists.
 */

//	****************************************************************************************
//	Concurrent Clean Standard Library Module Version 2.0
//	Copyright 1998 University of Nijmegen
//	****************************************************************************************

import StdClass

//* Sort a list (mergesort).
sort    ::               !u:[a] -> u:[a] | Ord a
	special
		a	= Char
		a	= Int
		a	= Real

/**
 * Sort a list using a custom ordering.
 * @param The custom {{`<`}} function
 */
sortBy  :: (a a -> Bool) !u:[a] -> u:[a]

//* Merge two sorted lists.
merge   ::               !u:[a] !v:[a] -> w:[a] | Ord a,[u v <= w]
	special
		a	= Char
		a	= Int
		a	= Real

/**
 * Merge two sorted lists using a custom ordering.
 * @param The custom {{`<`}} function
 */
mergeBy :: (a a -> Bool) !u:[a] !v:[a] -> w:[a],[u v <= w]

//* The maximum element of a list.
maxList	:: !.[a] 			-> a 		 | Ord a
	special
		a	= Char
		a	= Int
		a	= Real

/**
 * The maximum element of a list using a custom ordering.
 * @param The custom {{`<`}} function
 */
maxListBy :: (a a -> Bool) !.[a] -> a

//* The minimum element of a list.
minList	:: !.[a] 			-> a 		 | Ord a
	special
		a	= Char
		a	= Int
		a	= Real
/**
 * The minimum element of a list using a custom ordering.
 * @param The custom {{`<`}} function
 */
minListBy :: (a a -> Bool) !.[a] -> a
