definition module StdCharList

/**
 * Basic functions for manipulating lists of characters.
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 2.0
//	Copyright 1998 University of Nijmegen
// ****************************************************************************************

/**
 * Center-align a text with spaces, s.t. the right margin is 0 or 1 spaces
 * longer than the left margin.
 *
 * @param The minimum length of the result
 * @param The text to center
 * @result A list of max(|param 2|, param 1), with param 2 surrounded by spaces
 */
cjustify	:: !.Int ![.Char] -> .[Char]

/**
 * Left-align a text with spaces
 *
 * @param The minimum length of the result
 * @param The text to align
 * @result A list of max(|param 2|, param 1), with spaces appended to param 2
 */
ljustify	:: !.Int ![.Char] -> .[Char]

/**
 * Right-align a text with spaces
 *
 * @param The minimum length of the result
 * @param The text to align
 * @result A list of max(|param 2|, param 1), with spaces prepended to param 2
 */
rjustify	:: !.Int ![.Char] -> [Char]

/**
 * Concatenate a list of texts, interspersing it with newlines
 *
 * @param The texts
 * @result All texts concatenated, with newlines in between
 */
flatlines	:: ![[u:Char]] -> [u:Char]

/**
 * Split a text on newlines
 *
 * @param The text
 * @result A list of texts without newlines
 */
mklines 	:: ![Char] -> [[Char]]

/**
 * A list of a number of ' ' characters
 *
 * @param The number of characters
 * @result A list of param 1 spaces
 */
spaces		:: !.Int -> .[Char]
