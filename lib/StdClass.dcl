definition module StdClass

/**
 * Meta-classes with derived functions.
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 2.0
//	Copyright 1998 University of Nijmegen
// ****************************************************************************************

import StdOverloaded
from StdBool import not

//	Remark: derived class members are not implemented yet!
//	For the time-being, macro definitions are used for this purpose
//	This may cause misleading error messages in case of type errors 

//* Meta-class describing interval types with an absolute zero.
class PlusMin a | + , - , zero a

//* Meta-class describing ratio types.
class MultDiv a | * , / , one a

//* Meta-class describing arithmetical types.
class Arith a 	| PlusMin , MultDiv , abs , sign , ~ a 

//* Meta-class describing types that can be incremented and decremented.
class IncDec a	| + , - , one , zero a
where
  //* Increment a value by one.
  inc :: !a -> a | + , one a
  inc x :== x + one

  //* Decrement a value by one.
  dec :: !a -> a | - , one a
  dec x :== x - one

//* Meta-class describing types that can be enumerated.
class Enum a | < , IncDec a

/**
 * Equality.
 * @var The type for which values can be equated
 */
class Eq a | == a	
where
  /**
   * Inequality.
   * @result True iff the parameters are not equal
   */
  (<>) infix  4 :: !a	!a	->	Bool | Eq a
  (<>) x y :== not (x == y)

/**
 * Ordering.
 * @var The type that can be ordered.
 */
class Ord a	| < a
where
  /**
   * Greater than.
   * @result True iff the first value is strictly greater than the second value.
   */
  (>) infix  4 :: !a !a	-> Bool | Ord a
  (>) x y  :== y < x 

  /**
   * Smaller than or equal to.
   * @result True iff the first value is smaller than or equal to the second value.
   */
  (<=) infix 4 :: !a !a -> Bool | Ord a
  (<=) x y :== not (y<x)

  /**
   * Greater than or equal to.
   * @result True iff the first value is greater than or equal to the second value.
   */
  (>=) infix 4 :: !a !a -> Bool | Ord a
  (>=) x y :== not (x<y) 

  //* The minimum of two values.
  min::!a !a ->	a | Ord a
  min x y  :== case (x<y) of True = x; _ = y

  //* The maximum of two values.
  max::!a !a ->	a | Ord a
  max x y  :== case (x<y) of True = y; _ = x
