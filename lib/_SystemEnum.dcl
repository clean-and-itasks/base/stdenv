definition module _SystemEnum

/**
 * The underlying functions for dotdot expressions.
 *
 * This module must be imported if dotdot expressions are used.
 * Then, the following constructs can be used:
 *
 * - `[from .. ]`         -> `{{_from}} from`
 * - `[from .. to]`       -> `{{_from_to}} from to`
 * - `[from, then .. ]`   -> `{{_from_then}} from then`
 * - `[from, then .. to]` -> `{{_from_then_to}} from then to`
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 2.0
//	Copyright 1998 University of Nijmegen
// ****************************************************************************************

from StdClass import class Enum (..), class IncDec (..), class Ord (..),<=,inc,dec
from StdBool import not 
import StdInt,StdChar

/**
 * Generate a list of all values from a starting point.
 * Instead of `_from x`, you can use `[x..]`.
 *
 * @param The starting point
 * @result [x, x + one, x + one + one, x + one + one + one, ...]
 */
_from			::  a		-> .[a] | IncDec , Ord a	special a=Int; a=Char

/**
 * Generate a list of all values from a starting point to an endpoint.
 * Instead of `_from_to x y`, you can use `[x..y]`.
 *
 * @param The starting point
 * @param The endpoint
 * @result [x, inc x, inc (inc x), inc (inc (inc x)), ..., E] where E <= y
 */
_from_to		:: !a !a	-> .[a] | Enum a			special a=Int; a=Char

/**
 * Generate a list of all values from a starting point with a certain interval.
 * Instead of `_from_then x y`, you can use `[x,y..]`.
 *
 * @param The starting point
 * @param The next point
 * @result [x, y, x + (y - x) + (y - x), ...]
 */
_from_then		::  a  a	-> .[a] | Enum a			special a=Int; a=Char

/**
 * Generate a list of all values from a starting point to an endpoint with a
 * certain interval.
 * Instead of `_from_then_to x y z`, you can use `[x,y..z]`.
 *
 * @param The starting point
 * @param The next point
 * @param The endpoint
 * @result [x, y, x + (y - x) + (y - x), ..., E] where E <= z
 */
_from_then_to	:: !a !a !a	-> .[a] | Enum a			special a=Int; a=Char
