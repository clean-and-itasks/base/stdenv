definition module _SystemArray

/**
 * Operations on arrays.
 * This is an internal class used to overload operations on different types of
 * arrays. It should not be instantiated outside of this module.
 */
class Array .a e where
	/**
	 * Select an element from an array.
	 * This is the function underlying the {{`.[]`}} construct.
	 * Also see {{`uselect`}}.
	 */
	select				:: !.(a .e) !Int	-> .e

	/**
	 * Select an element from a unique array.
	 * This is the function underlying the {{`![]`}} construct.
	 * Also see {{`select`}}.
	 */
	uselect				:: !u:(a e) !Int	-> *(e, !u:(a e))

	/**
	 * The size of an array.
	 * Also see {{`usize`}}.
	 */
	size				:: !.(a .e)			-> Int

	/**
	 * The size of a unique array.
	 * Also see {{`size`}}.
	 */
	usize				:: !u:(a .e)		-> *(!Int, !u:(a .e))

	/**
	 * Update an element in a unique array.
	 * This is the function underlying the {{`&`}} construct.
	 * Also see {{`replace`}}.
	 * @param The array
	 * @param The index to update
	 * @param The new element
	 * @result The new array
	 */
	update				:: !*(a .e) !Int .e -> *(a .e)	

	/**
	 * Create a new array. Also see {{`_createArray`}}.
	 * @param The number of elements
	 * @param The elements
	 * @result The array
	 */
	createArray			:: !Int e			-> *(a e)

	/**
	 * Create an empty array of a certain size. Also see {{`createArray`}}.
	 * @param The number of elements
	 * @result The array
	 */
	_createArray		:: !Int				-> *(a .e)

	/**
	 * Replace an element in an array and return the previous value.
	 * Also see {{`update`}}.
	 * @param The array
	 * @param The index to update
	 * @param The new value
	 * @result The old value
	 * @result The new array
	 */
	replace				:: !*(a .e) !Int .e -> *(.e, !*(a .e))

instance Array {!} a where
	uselect :: !u:{! e} !Int -> *(!e, !u:{! e})
	update :: !*{! .e} !Int !.e -> *{! .e}
	createArray :: !Int !e -> *{! e}
	replace :: !*{! .e} !Int !.e -> *(!.e, !*{! .e})

instance Array {#} Int where
	uselect :: !u:{# Int} !Int -> *(!Int, !u:{# Int})
	update :: !*{# e:Int} !Int !e:Int -> *{# e:Int}
	createArray :: !Int !Int -> *{# Int}
	replace :: !*{# e:Int} !Int !e:Int -> *(!e:Int, !*{# e:Int})

instance Array {#} Char where
	uselect :: !u:{# Char} !Int -> *(!Char, !u:{# Char})
	update :: !*{# e:Char} !Int !e:Char -> *{# e:Char}
	createArray :: !Int !Char -> *{# Char}
	replace :: !*{# e:Char} !Int !e:Char -> *(!e:Char, !*{# e:Char})

instance Array {#} Real where
	uselect :: !u:{# Real} !Int -> *(!Real, !u:{# Real})
	update :: !*{# e:Real} !Int !e:Real -> *{# e:Real}
	createArray :: !Int !Real -> *{# Real}
	replace :: !*{# e:Real} !Int !e:Real -> *(!e:Real, !*{# e:Real})

instance Array {#} Bool where
	uselect :: !u:{# Bool} !Int -> *(!Bool, !u:{# Bool})
	update :: !*{# e:Bool} !Int !e:Bool -> *{# e:Bool}
	createArray :: !Int !Bool -> *{# Bool}
	replace :: !*{# e:Bool} !Int !e:Bool -> *(!e:Bool, !*{# e:Bool})

instance Array {#} {#.a} where
	uselect :: !u:{#{#.a}} !Int -> *(!{#.a},!u:{#{#.a}})
	update :: !*{#u:{#.a}} !Int !u:{#.a} -> *{#u:{#.a}}
	createArray :: !Int !{#.a} -> *{# {#.a}}
	replace :: !*{#u:{#.a}} !Int !u:{#.a} -> *(!u:{#.a},!*{#u:{#.a}})

instance Array {#} {!.a} where
	uselect :: !u:{#{!.a}} !Int -> *(!{!.a},!u:{#{!.a}})
	update :: !*{#u:{!.a}} !Int !u:{!.a} -> *{#u:{!.a}}
	createArray :: !Int !{!.a} -> *{# {!.a}}
	replace :: !*{#u:{!.a}} !Int !u:{!.a} -> *(!u:{!.a},!*{#u:{!.a}})

instance Array {#} {.a} where
	uselect :: !u:{#{.a}} !Int -> *(!{.a},!u:{#{.a}})
	update :: !*{#u:{.a}} !Int !u:{.a} -> *{#u:{.a}}
	createArray :: !Int !{.a} -> *{# {.a}}
	replace :: !*{#u:{.a}} !Int !u:{.a} -> *(!u:{.a},!*{#u:{.a}})

instance Array {#} {32#.a} where
	uselect :: !u:{#{32#.a}} !Int -> *(!{32#.a},!u:{#{32#.a}})
	update :: !*{#u:{32#.a}} !Int !u:{32#.a} -> *{#u:{32#.a}}
	createArray :: !Int !{32#.a} -> *{# {32#.a}}
	replace :: !*{#u:{32#.a}} !Int !u:{32#.a} -> *(!u:{32#.a},!*{#u:{32#.a}})

instance Array {#} a where
	uselect :: !u:{# e} !Int -> *(!e, !u:{# e})
	update :: !*{# .e} !Int !.e -> *{# .e}
	createArray :: !Int !e -> *{# e}
	replace :: !*{# .e} !Int !.e -> *(!.e, !*{# .e})

instance Array {32#} Int where
	uselect :: !u:{32# Int} !Int -> *(!Int, !u:{32# Int})
	update :: !*{32# e:Int} !Int !e:Int -> *{32# e:Int}
	createArray :: !Int !Int -> *{32# Int}
	replace :: !*{32# e:Int} !Int !e:Int -> *(!e:Int, !*{32# e:Int})

instance Array {32#} Real where
	uselect :: !u:{32# Real} !Int -> *(!Real, !u:{32# Real})
	update :: !*{32# e:Real} !Int !e:Real -> *{32# e:Real}
	createArray :: !Int !Real -> *{32# Real}
	replace :: !*{32# e:Real} !Int !e:Real -> *(!e:Real, !*{32# e:Real})

instance Array {32#} a where
	uselect :: !u:{32# e} !Int -> *(!e, !u:{32# e})
	update :: !*{32# .e} !Int !.e -> *{32# .e}
	createArray :: !Int !e -> *{32# e}
	replace :: !*{32# .e} !Int !.e -> *(!.e, !*{32# .e})

instance Array {} a
