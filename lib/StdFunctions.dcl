definition module StdFunctions

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 3.0
//	Copyright 2018 Radboud University
// ****************************************************************************************

//* The identity function.
id    :: !.a -> .a
//* Always returns the first argument.
const :: !.a .b -> .a

/**
 * Flips the arguments of a function. This is useful in function compositions.
 * @type !.(.a -> .(.b -> .c)) .b .a -> .c
 */
flip f a b :== f b a

/**
 * Function composition: apply `f` after `g`.
 * @type u:(.a -> .b) u:(.c -> .a) -> u:(.c -> .b)
 */
(o) infixr  9
(o) f g :== \ x -> f (g x)

//* Apply the function argument twice.
twice			:: !(.a -> .a)   .a             -> .a
//* Apply the second argument as long as the first argument holds.
while			:: !(a -> .Bool) (a -> a) 	 a 	->  a
//* Apply the second argument until the first argument holds.
until			:: !(a -> .Bool) (a -> a) 	 a 	->  a
//* Apply a function a number of times.
iter			:: !Int 		 (.a -> .a) .a	-> .a
