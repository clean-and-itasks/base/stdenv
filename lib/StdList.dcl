definition module StdList

/**
 * Basic operations on lists.
 */

//	****************************************************************************************
//	Concurrent Clean Standard Library Module Version 2.2
//	Copyright 1998-2006 University of Nijmegen
//	****************************************************************************************

import StdClass
import StdInt,StdChar,StdReal

//	Instances of overloaded functions:

instance ==	[a] | == a
						special a=Int
								a=Char
								a=Real

instance <	[a] | Ord a
						special a=Int
								a=Char
								a=Real

instance length	[]
instance %		[a]

instance toString 	[x] | toChar x	 // Convert [x]    via [Char] into String
									special x=Char
instance fromString [x] | fromChar x // Convert String via [Char] into [x]
									special x=Char

//	List Operators:

//* The nth element of a list.
(!!) 	infixl 9	:: ![.a] !Int -> .a

//* Concatenate two lists.
(++)	infixr 5	:: ![.a] u:[.a] -> u:[.a]

//* Concatenate a list of lists.
flatten				:: ![[.a]] -> [.a]

//* True iff the list is empty.
isEmpty				:: ![.a] -> Bool

//	List breaking or permuting functions:

//* The head (first element) of a list.
hd			:: ![.a] -> .a

//* The tail (everything except the first element) of a list.
tl			:: !u:[.a] -> u:[.a]

//* The last element of a list.
last		:: ![.a] -> .a

//* Everything except the last element of a list.
init	 	:: ![.a] -> [.a]

/**
 * Take a number of elements from the start of a list. When there are not
 * enough elements, the original list is returned.
 */
take		:: !Int [.a] -> [.a]

//* Take elements from the start of a list as long as a predicate holds.
takeWhile	:: (a -> .Bool) !.[a] -> .[a]

/**
 * Drop a number of elements from the start of a list. When there are not
 * enough elements, `[]` is returned.
 */
drop		:: !Int !u:[.a] -> u:[.a]

//* Drop elements from the start of a list as long as a predicate holds.
dropWhile	:: (a -> .Bool) !u:[a] -> u:[a]

/**
 * @param A predicate `p`
 * @param A list `xs`
 * @result `{{takeWhile}} p xs`
 * @result `{{dropWhile}} p xs`
 */
span		:: (a -> .Bool) !u:[a] -> (.[a],u:[a])

/**
 * Filter a list using a predicate.
 * @result The list containing exactly those elements for which the predicate
 *   holds.
 */
filter		:: (a -> .Bool) !.[a] -> .[a]

//* Reverse a list.
reverse		:: ![.a] -> [.a]

/**
 * Insert an element when a certain predicate holds. When the predicate does
 * not hold for any element, the new element is added to the end of the list.
 * @param The predicate on the new element and the existing element
 * @param The new element
 * @param The list
 */
insert 		:: (a -> a -> .Bool) a !u:[a] -> u:[a]

/**
 * Insert an element at an index. When the index is out-of-range, add it to the
 * end of the list.
 */
insertAt	:: !Int .a u:[.a] -> u:[.a]

/**
 * Remove an element at a certain index from a list. When the index is
 * out-of-range, the original list is returned.
 */
removeAt	:: !Int !u:[.a] -> u:[.a]

/**
 * Replace an element at a certain index. When the index is out-of-range, the
 * original list is returned.
 */
updateAt 	:: !Int .a !u:[.a] -> u:[.a]

/**
 * @param An index `n` of the list
 * @param The list `xs`
 * @result `{{take}} n xs`
 * @result `{{drop}} n xs`
 */
splitAt		:: !Int u:[.a] -> ([.a],u:[.a])			//	(take n list,drop n list)

//	Creating lists:

//* Apply a function to every element of a list.
map			:: (.a -> .b) ![.a] -> [.b]

/**
 * Repeatedly apply a function.
 * @param The function `f`
 * @param The initial value `x`
 * @result `[x, f x, f (f x), f (f (f x)), ...]`
 */
iterate		:: (a -> a) a -> .[a]

/**
 * Create a list of indices for an existing list.
 * @param The list `xs`
 * @result `[0..{{length}} xs - 1]`
 */
indexList	:: !.[a] -> [Int]

//* Create a list of `n` elements `x`.
repeatn		:: !.Int a -> .[a]

//* Repeat an element indefinitely.
repeat		:: a -> [a]

//* Unzip a list of tuples to a tuple of lists.
unzip		::	![(.a,.b)] 		-> ([.a],[.b])

/**
 * Zip two lists to a list of tuples. The result list has the length of the
 * shortest of the two original lists.
 * Also see {{`zip`}}.
 */
zip2		:: ![.a] [.b] 		-> [(.a,.b)]

//* Same as {{`zip2`}}, but for two lists in a tuple.
zip			:: !(![.a],[.b]) 	-> [(.a,.b)]

//* Diagonally create the Cartesian product for two lists.
diag2		:: !.[a] .[b]		-> [.(a,b)]

//* Diagonally create the Cartesian product for three lists.
diag3		:: !.[a] .[b] .[c]	-> [.(a,b,c)]

//	Folding and scanning:

/**
 * Replace all list constructors with a left-associative operator.
 * For efficiency reasons, `foldl` is a macro, so that applications of this
 * functions are inlined.
 * Also see `foldr`.
 *
 * @param The operator (substitute for `Cons`)
 * @param The substitute for `Nil`
 * @param The list
 * @result `op (... (op (op (op r e0) e1) e2) ...) en`
 * @type (.a -> .(.b -> .a)) .a ![.b] -> .a
 */
foldl op r l :== foldl r l
	where
		foldl r []		= r
		foldl r [a:x]	= foldl (op r a) x

/**
 * Replace all list constructors with a right-associative operator.
 * For efficiency reasons, `foldr` is a macro, so that applications of this
 * functions are inlined.
 * Also see `foldl`.
 *
 * @param The operator (substitute for `Cons`)
 * @param The substitute for `Nil`
 * @param The list
 * @result `op e0 (op e1 (... (op en r) ...))`
 * @type (.a -> .(.b -> .b)) .b ![.a] -> .b
 */
foldr op r l :== foldr l
	where
		foldr []	= r
		foldr [a:x]	= op a (foldr x)

/**
 * `scan` is like {{`foldl`}} but returns a list of reduced values rather than
 * only the end result.
 *
 * @param An operator `op`
 * @param An initial value `r`
 * @param A list `xs = [e0, e1, e2, ...]`
 * @result `[r, op r e0, op (op r e0) e1, ...]`
 */
scan		::  (a -> .(.b -> a)) a ![.b] -> .[a]

//	On Booleans

//* Check that all booleans are {{`True`}}. Also see {{`all`}}.
and			:: ![.Bool] -> Bool

//* Check that at least one boolean is {{`True`}}. Also see {{`any`}}.
or			:: ![.Bool] -> Bool

//* Check that a predicate holds for at least one list element.
any			:: (.a -> .Bool) ![.a] -> Bool

//* Check that a predicate holds for all list elements.
all			:: (.a -> .Bool) ![.a] -> Bool

//	When equality is defined on list elements

//* Check if an element is in the list.
isMember		::    a	 !.[a]	-> Bool 	| Eq a
													special a=Int
															a=Char
															a=Real
//* Check that the intersection between two lists is not empty.
isAnyMember		:: !.[a]  !.[a] -> Bool 	| Eq a
													special a=Int
															a=Char
															a=Real
//* Remove the first occurrence of an element from the list.
removeMember	:: a !u:[a] -> u:[a] 		| Eq a
													special a=Int
															a=Char
															a=Real
/**
 * Remove the first occurrences of the elements of the second list from the
 * first list.
 */
removeMembers	:: !u:[a] !.[a]	-> u:[a] 	| Eq a
													special a=Int
															a=Char
															a=Real
//* Remove all duplicates from a list.
removeDup		:: !.[a] 		-> .[a] 	| Eq a
													special a=Int
															a=Char
															a=Real
/**
 * Remove the first occurrence of an element from a list.
 * @result The index of the removed element
 * @result The new list
 */
removeIndex 	:: !a !u:[a] -> (Int,u:[a])	| Eq a
													special a=Int
															a=Char
															a=Real
/**
 * Find the first element for which the next element is the same.
 * E.g., `limit [1,3,2,2,1,...] = 2`
 */
limit			:: !.[a] 		-> a 		| Eq a
													special a=Int
															a=Char
															a=Real

//	When addition is defined on list elements

//* Overloaded sum on a list. The sum of `[]` is {{`zero`}}.
sum :: !.[a] -> a |  + , zero  a
									special	a=Int
											a=Real

//	When multiplication and addition is defined on list elements

//* Overloaded product of a list. The product of `[]` is {{`one`}}.
prod :: !.[a] -> a | * , one  a
									special	a=Int
											a=Real
//* Overloaded average of a list. The average of `[]` gives a runtime error.
avg :: !.[a] -> a | / , IncDec a
									special	a=Int
											a=Real
														

