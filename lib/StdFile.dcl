definition module StdFile

/**
 * Functions to manipulate the file system with the File type.
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 3.0
//	Copyright 2019 University of Nijmegen
// ****************************************************************************************

//	File modes synonyms

//* File mode: read text
FReadText	:== 0

//* File mode: write text
FWriteText	:== 1

//* File mode: append text
FAppendText	:== 2

//* File mode: read data
FReadData	:== 3

//* File mode: write data
FWriteData	:== 4

//* File mode: append data
FAppendData	:== 5

//	Seek modes synonyms

//* Seek mode: the new position is the seek offset
FSeekSet	:== 0

//* Seek mode: the new position is the current position plus the seek offset
FSeekCur	:== 1

//* Seek mode: the new position is the size of the file plus the seek offset
FSeekEnd	:== 2

/**
 * The filesystem environment, independent from *World. This type can only be
 * used through the FileSystem and FileEnv classes.
 */
:: *Files

/**
 * Access to the filesystem.
 *
 * @var The unique type that is used to ensure purity.
 */
class FileSystem f where
	/**
	 * Opens a file for the first time in a certain mode.
	 * @param The filename
	 * @param The mode (read / write / append; text / data)
	 * @param The {{`FileSystem`}} (usually {{`World`}})
	 * @result A boolean indicating success
	 * @result The {{`File`}}
	 * @result The new {{`FileSystem`}}
	 */
	fopen :: !{#Char} !Int !*f -> (!Bool,!*File,!*f)

	/**
	 * Closes a file.
	 * @param The {{`File`}}
	 * @param The {{`FileSystem`}}
	 * @result A boolean indicating success
	 * @result The new {{`FileSystem`}}
	 */
	fclose :: !*File !*f -> (!Bool,!*f)

	//* Open the 'Console' for reading and writing.
	stdio  :: !*f -> (!*File,!*f)

	/**
	 * With `sfopen` a file can be opened for reading more than once. On a file
	 * opened by `sfopen` only the operations beginning with `sf` can be used.
	 * The `sf...` operations work just like the corresponding `f...`
	 * operations. They can't be used for files opened with {{`fopen`} or
	 * {{`freopen`}}.
	 *
	 * @param The filename
	 * @param The mode (read; text / data)
	 * @param The {{`FileSystem`}} (usually {{`World`}})
	 * @result A boolean indicating success
	 * @result The new {{`File`}}
	 * @result The new {{`FileSystem`}}
	 */
	sfopen :: !{#Char} !Int !*f -> (!Bool,!File,!*f)

instance FileSystem Files
instance FileSystem World

/**
 * An environment in which files can be dealt with.
 *
 * @var The unique type that is used to ensure purity.
 */
class FileEnv env where
	accFiles :: !.(*Files -> (.x,*Files)) !*env -> (!.x,!*env)
	appFiles :: !.(*Files -> *Files) !*env -> *env

instance FileEnv World

/**
 * Re-opens an open file in a possibly different mode.
 * @param The file
 * @param The new mode
 * @result A boolean indicating successful closing before reopening
 * @result The new file
 */
freopen		:: !*File !Int -> (!Bool,!*File)							:== code { .d 0 3 f i ; jsr reopenF ; .o 0 3 b f }

//	Reading from a File:

/**
 * Reads a character from a text file or a byte from a datafile.
 * @result A boolean indicating success
 * @result The read character
 */
freadc		:: !*File -> (!Bool,!Char,!*File)							:== code { .d 0 2 f ; jsr readFC ; .o 0 4 b c f }

/**
 * Reads an Integer from a textfile by skipping spaces, tabs and newlines and
 * then reading digits, which may be preceeded by a plus or minus sign.
 * From a datafile `freadi` will just read four bytes (a Clean Int).
 * @result A boolean indicating success
 * @result The read integer
 */
freadi		:: !*File -> (!Bool,!Int,!*File)							:== code { .d 0 2 f ; jsr readFI ; .o 0 4 b i f }

/**
 * Reads a Real from a textfile by skipping spaces, tabs and newlines and then
 * reading a character representation of a Real number.
 * From a datafile `freadr` will just read eight bytes (a Clean Real).
 * @result A boolean indicating success
 * @result The read real
 */
freadr		:: !*File -> (!Bool,!Real,!*File)							:== code { .d 0 2 f ; jsr readFR ; .o 0 5 b r f }

/**
 * Reads n characters from a text or data file, which are returned as a String.
 * If the file doesn't contain n characters the file will be read to the end
 * of the file. An empty String is returned if no characters can be read.
 * @param The file
 * @param The amount of characters to read
 * @result The read string
 * @result The file
 */
freads		:: ! *File !Int -> (!*{#Char},!*File)						:== code { .d 0 3 f i ; jsr readFS ; .o 1 2 f }

/**
 * Reads `n` characters from a text or data file, which are returned in the
 * string `arg3` at positions `arg1`..`arg1+arg2-1`. If the file doesn't
 * contain `arg2` characters the file will be read to the end of the file, and
 * the part of the string `arg3` that could not be read will not be changed.
 *
 * @param The start of the substring to modify
 * @param The length of the substring
 * @param The string to modify
 * @result The number of characters read
 * @result The modified string
*/
freadsubstring :: !Int !Int !*{#Char} !*File -> (!Int,!*{#Char},!*File)	:== code { .d 1 4 i i f ; jsr readFString ; .o 1 3 i f }

/**
 * Reads a line from a textfile, including a newline character, except for the
 * last line. `freadline` cannot be used on data files.
 */
freadline	:: !*File -> (!*{#Char},!*File)								:== code { .d 0 2 f ; jsr readLineF ; .o 1 2 f }

//	Writing to a File:

/**
 * Writes a character to a textfile.
 * To a datafile fwritec writes one byte (a Clean Char).
 */
fwritec		:: !Char !*File -> *File									:== code { .d 0 3 c f ; jsr writeFC ; .o 0 2 f }

/**
 * Writes an Integer (its textual representation) to a text file. To a datafile
 * fwritei writes four bytes (a Clean Int).
 */
fwritei		:: !Int !*File -> *File										:== code { .d 0 3 i f ; jsr writeFI ; .o 0 2 f }

/**
 * Writes a Real (its textual representation) to a text file. To a datafile
 * fwriter writes eight bytes (a Clean Real).
 */
fwriter		:: !Real !*File -> *File									:== code { .d 0 4 r f ; jsr writeFR ; .o 0 2 f }

//* Writes a String to a text or data file.
fwrites		:: !{#Char} !*File -> *File									:== code { .d 1 2 f ; jsr writeFS ; .o 0 2 f }

/**
 * Writes the characters at positions `arg1`..`arg1+arg2-1` of string `arg3` to
 * a text or data file.
 */
fwritesubstring :: !Int !Int !{#Char} !*File -> *File					:== code { .d 1 4 i i f ; jsr writeFString ; .o 0 2 f }

/**
 * Overloaded write to file. This allows you to chain write operations, like:
 * `# f = f <<< "X is: " <<< x <<< "; y is: " <<< y <<< "\n"`
 *
 * @var The type that can be written to a file
 * @param The File
 * @param The thing to write
 * @result The new File
 */
class (<<<) infixl a :: !*File !a -> *File

instance <<< Int 		:: !*File !Int -> *File							:== code { push_b 2 ; update_b 2 3 ; update_b 1 2 ; updatepop_b 0 1 ; .d 0 3 i f ; jsr writeFI ; .o 0 2 f }
instance <<< Char		:: !*File !Char -> *File						:== code { push_b 2 ; update_b 2 3 ; update_b 1 2 ; updatepop_b 0 1 ; .d 0 3 c f ; jsr writeFC ; .o 0 2 f }
instance <<< {#Char}	:: !*File !{#Char} -> *File						:== code { .d 1 2 f ; jsr writeFS ; .o 0 2 f }
instance <<< Real		:: !*File !Real -> *File						:== code { push_b 3 ; push_b 3 ; update_b 3 5 ; update_b 2 4 ; update_b 1 3 ; updatepop_b 0 2 ; .d 0 4 r f ; jsr writeFR ; .o 0 2 f }

//	Testing:

/**
 * @result Whether end-of-file has been reached
 */
fend		:: !*File -> (!Bool,!*File)									:== code { .d 0 2 f ; jsr endF ; .o 0 3 b f }

/**
 * @result Whether an error has occurred during previous file I/O operations
 */
ferror		:: !*File -> (!Bool,!*File)									:== code { .d 0 2 f ; jsr errorF ; .o 0 3 b f }

/**
 * @result The current position of the file pointer as an Integer. This
 *   position can be used later on for the fseek function.
 */
fposition	:: !*File -> (!Int,!*File)									:== code { .d 0 2 f ; jsr positionF ; .o 0 3 i f }

/**
 * Move to a different position in the file
 *
 * @param The offset
 * @param A seek mode ({{`FSeekSet`}}, {{`FSeekCur`}} or {{`FSeekEnd`}})
 * @result True iff the seek was successful
 */
fseek		:: !*File !Int !Int -> (!Bool,!*File)						:== code { .d 0 4 f i i ; jsr seekF ; .o 0 3 b f }

//	Predefined files.

//* Open the 'Errors' file for writing only. May be opened more than once.
stderr		:: *File													:== code { .d 0 0 ; jsr	stderrF ; .o 0 2 f }

//	Opening and reading Shared Files:

sfreadc		:: !File -> (!Bool,!Char,!File)								:== code { .d 0 2 f ; jsr readSFC ; .o 0 4 b c f }
sfreadi		:: !File -> (!Bool,!Int,!File)								:== code { .d 0 2 f ; jsr readSFI ; .o 0 4 b i f }
sfreadr		:: !File -> (!Bool,!Real,!File)								:== code { .d 0 2 f ; jsr readSFR ; .o 0 5 b r f }
sfreads		:: !File !Int -> (!*{#Char},!File)							:== code { .d 0 3 f i ; jsr readSFS ; .o 1 2 f }
sfreadline	:: !File -> (!*{#Char},!File)								:== code { .d 0 2 f ; jsr readLineSF ; .o 1 2 f }
sfseek		:: !File !Int !Int -> (!Bool,!File)							:== code { .d 0 4 f i i ; jsr seekSF ; .o 0 3 b f }

sfend		:: !File -> Bool											:== code { .d 0 2 f ; jsr endSF ; .o 0 1 b }
sfposition	:: !File -> Int												:== code { .d 0 2 f ; jsr positionSF ; .o 0 1 i }
/*	The functions sfend and sfposition work like fend and fposition, but don't return a
	new file on which other operations can continue. They can be used for files opened
	with sfopen or after fshare, and in guards for files opened with fopen or freopen. */

//	Convert a *File into:

//* Change a file so that from now it can only be used with `sf...` operations.
fshare		:: !*File -> File											:== code { .d 0 2 f ; jsr shareF ; .o 0 2 f }

//* Flush all I/O operations on a file.
fflush :: !*File -> (!Bool,!*File)										:== code { .d 0 2 f ; jsr flushF ; .o 0 3 bf }
