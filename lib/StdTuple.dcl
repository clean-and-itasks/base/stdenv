definition module StdTuple

/**
 * Class instances and basic functions for tuples.
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 2.0
//	Copyright 1998 University of Nijmegen
// ****************************************************************************************

import StdClass

/**
 * The first element of a two-tuple.
 *
 * @type !(!.a,.b) -> .a
 * @param The tuple
 * @result The first element
 */
fst tuple :== t1 where (t1, _) = tuple

/**
 * The second element of a two-tuple.
 *
 * @type !(.a,!.b) -> .b
 * @param The tuple
 * @result The second element
 */
snd tuple :== t2 where (_, t2) = tuple

/**
 * The first element of a three-tuple.
 *
 * @type !(!.a,.b,.c) -> .a
 * @param The tuple
 * @result The first element
 */
fst3 tuple :== t1 where (t1, _, _) = tuple

/**
 * The second element of a three-tuple.
 *
 * @type !(.a,!.b,.c) -> .b
 * @param The tuple
 * @result The second element
 */
snd3 tuple :== t2 where (_, t2, _) = tuple

/**
 * The third element of a three-tuple.
 *
 * @type !(.a,.b,!.c) -> .c
 * @param The tuple
 * @result The third element
 */
thd3 tuple :== t3 where (_, _, t3) = tuple

instance == ()                              :: !() !() -> Bool :== code { pop_a 2 ; pushB TRUE }
instance == (a,b)   | Eq a & Eq b			:: !(!a,b) !(!a,b) -> Bool | Eq a & Eq b
instance == (a,b,c) | Eq a & Eq b & Eq c	:: !(!a,b,c) !(!a,b,c) -> Bool | Eq a & Eq b & Eq c

instance <  ()                              :: !() !() -> Bool :== code { pop_a 2 ; pushB FALSE }
instance <  (a,b)   | Ord a & Ord b			:: !(!a,b) !(!a,b) -> Bool | Ord a & Ord b
instance <  (a,b,c) | Ord a & Ord b & Ord c	:: !(!a,b,c) !(!a,b,c) -> Bool | Ord a & Ord b & Ord c

/**
 * Apply functions to both elements of a two-tuple.
 *
 * @param A tuple of functions (f,g)
 * @param A tuple of values (x,y)
 * @result The tuple of the result of the applications (f x,g y)
 */
app2 	:: !(.(.a -> .b),.(.c -> .d)) !(.a,.c) -> (.b,.d)

/**
 * Apply functions to all elements of a three-tuple.
 *
 * @param A tuple of functions (f,g,h)
 * @param A tuple of values (x,y,z)
 * @result The tuple of the result of the applications (f x,g y,h z)
 */
app3 	:: !(.(.a -> .b),.(.c -> .d),.(.e -> .f)) !(.a,.c,.e) -> (.b,.d,.f)

/**
 * Apply a function that takes a two-tuple to two separate values.
 *
 * @param A function f
 * @param The first element of the parameter tuple x
 * @param The second element of the parameter tuple y
 * @result f applied to (x,y)
 */
curry	:: !.((.a,.b) -> .c) .a .b -> .c

/**
 * Apply a function for two separate values to a two-tuple.
 *
 * @param A function f
 * @param A tuple of parameters (x,y)
 * @result The application f x y.
 */
uncurry :: !.(.a -> .(.b -> .c)) !(.a,.b) -> .c
