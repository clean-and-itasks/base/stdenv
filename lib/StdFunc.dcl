definition module StdFunc

/**
 * A number of general functions and functions dealing with functions.
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 3.0
//	Copyright 2018 Radboud University
// ****************************************************************************************

import StdFunctions

//	Some handy functions for transforming unique states:

/**
 * Iterate a list of state functions.
 *
 * @param The functions
 * @param The initial state
 * @result The final state
 */
seq				:: ![.(.s -> .s)] .s -> .s

/**
 * Iterate a list of state functions with result
 *
 * @param The functions
 * @param The initial state
 * @result A list of results from the state function and the final state
 */
seqList			:: ![St .s .a] .s -> ([.a],.s)

//* A function that updates a state and produces a result.
:: St s a :== s -> *(a,s)

// monadic style:

/**
 * Monadic bind for the {{`St`}} type.
 * @type w:(St .s .a) v:(.a -> .(St .s .b)) -> u:(St .s .b), [u <= v, u <= w]
 */
(`bind`) infix 0
(`bind`) f g :== \st0 -> let (r,st1) = f st0 in g r st1

/**
 * Monadic return for the {{`St`}} type.
 * @type u:a -> u:(St .s u:a)
 */
return r :== \s -> (r,s)
