definition module StdEnum

/**
 * This module must be imported if dotdot expressions are used.
 * Then, the following constructs can be used:
 *
 * - `[from .. ]`         -> `{{_from}} from`
 * - `[from .. to]`       -> `{{_from_to}} from to`
 * - `[from, then .. ]`   -> `{{_from_then}} from then`
 * - `[from, then .. to]` -> `{{_from_then_to}} from then to`
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 2.0
//	Copyright 1998 University of Nijmegen
// ****************************************************************************************

import	_SystemEnum
