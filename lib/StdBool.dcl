definition module StdBool

/**
 * Class instances and basic functions for the Bool type.
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 3.0
//	Copyright 2019 University of Nijmegen
// ****************************************************************************************

import	StdOverloaded

instance ==				Bool	:: !Bool !Bool -> Bool				:== code { eqB }

instance toBool			Bool	:: !Bool -> Bool					:== code { no_op }

instance fromBool		Bool	:: !Bool -> Bool					:== code { no_op }
instance fromBool		{#Char}	:: !Bool -> {#Char}					:== code { .d 0 1 b ; jsr BtoAC ; .o 1 0 }

//	Additional Logical Operators:

/**
 * Logical negation.
 *
 * @param The boolean to negate
 * @result True if the parameter was False; False if True
 */
not					:: !Bool		->	Bool						:== code { notB }

/**
 * Logical disjunction. The second parameter is not strict and will not be
 * evaluated if the first parameter is True.
 *
 * @param First boolean
 * @param Second boolean
 * @result True iff at least one of the parameters is True
 */
(||)	infixr 2	:: !Bool Bool	->	Bool

/**
 * Logical conjunction. The second parameter is not strict and will not be
 * evaluated if the first parameter is False.
 *
 * @param First boolean
 * @param Second boolean
 * @result True iff both parameters are True
 */
(&&)	infixr 3	:: !Bool Bool	->	Bool
