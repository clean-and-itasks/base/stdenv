definition module StdChar

/**
 * Class instances and basic functions for the Char type.
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 3.0
//	Copyright 2019 University of Nijmegen
// ****************************************************************************************

import	StdOverloaded

instance + 				Char	:: !Char !Char -> Char		:== code { addI ; ItoC }
instance - 				Char	:: !Char !Char -> Char		:== code { subI; ItoC }
instance zero 			Char	:: Char						:== code { pushI 0; ItoC }
instance one 			Char	:: Char						:== code { pushI 1; ItoC }

instance ==				Char	:: !Char !Char -> Bool		:== code { eqC }
instance <  			Char	:: !Char !Char -> Bool		:== code { ltC }

instance toChar			Char	:: !Char -> Char			:== code { no_op }
instance toChar			Int		:: !Int -> Char				:== code { ItoC }

instance fromChar		Int		:: !Char -> Int				:== code { CtoI }
instance fromChar		Char	:: !Char -> Char			:== code { no_op }
instance fromChar		{#Char}	:: !Char -> {#Char}			:== code { CtoAC }

//	Additional conversions:

/**
 * Converts a character from ['0'..'9'] to an integer.
 *
 * @param The character
 * @result 0-9 if the character was from ['0'..'9'], otherwise another Int
 */
digitToInt		:: !Char -> Int

/**
 * Converts a character to uppercase.
 *
 * @param The character
 * @result The same character, with bit 5 cleared
 */
toUpper			:: !Char -> Char

/**
 * Converts a character to lowercase.
 *
 * @param The character
 * @result The same character, with bit 5 set
 */
toLower			:: !Char -> Char

//	Tests on Characters:

/**
 * Check for uppercase
 *
 * @param The character
 * @result True iff the parameter is from ['A'..'Z']
 */
isUpper			:: !Char -> Bool

/**
 * Check for lowercase
 *
 * @param The character
 * @result True iff the parameter is from ['a'..'z']
 */
isLower			:: !Char -> Bool

/**
 * Check for an alphabetic character
 *
 * @param The character
 * @result True iff the parameter is from ['a'..'z'] ++ ['A'..'Z']
 */
isAlpha			:: !Char -> Bool

/**
 * Check for an alphabetic or numerical character
 *
 * @param The character
 * @result True iff the parameter is from ['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9']
 */
isAlphanum		:: !Char -> Bool

/**
 * Check for a numerical character
 *
 * @param The character
 * @result True iff the parameter is from ['0'..'9']
 */
isDigit			:: !Char -> Bool

/**
 * Check for an octal digit
 *
 * @param The character
 * @result True iff the parameter is from ['0'..'7']
 */
isOctDigit		:: !Char -> Bool

/**
 * Check for a hexadecimal digit
 *
 * @param The character
 * @result True iff the parameter is from ['0'..'9'] ++ ['a'..'f'] ++ ['A'..'F']
 */
isHexDigit		:: !Char -> Bool

/**
 * Check for whitespace
 *
 * @param The character
 * @result True iff the parameter is one of ' ', '\t', '\n', '\r', '\f', '\v'
 */
isSpace			:: !Char -> Bool

/**
 * Check for an ASCII control character
 *
 * @param The character
 * @result True iff the parameter is from ['\x00'..'\x1f'] ++ ['\x7f']
 */
isControl		:: !Char -> Bool

/**
 * Check for a printable ASCII character
 *
 * @param The character
 * @result True iff the parameter is from [' '..'~']
 */
isPrint			:: !Char -> Bool

/**
 * Check for a 7-bit ASCII character
 *
 * @param The character
 * @result True iff the integer value of the parameter is less than 128
 */
isAscii			:: !Char -> Bool
