definition module StdDebug

/**
 * Functions that write intermediate results to stderr for debugging purposes.
 */

// ********************************************************
//	Concurrent Clean Standard Library Module Version 2.0
//	Copyright 1998 University of Nijmegen
// ********************************************************

import StdClass

from StdString import instance toString	{#Char},instance toString Int

// The following functions should only be used for debugging,
// because these functions have side effects

/**
 * Write a message to stderr before returning a value.
 *
 * @param The message to write to stderr
 * @param The value to return
 * @result Param 2
 */
trace :: !msg .a -> .a | toString msg special msg={#Char}; msg=Int

/**
 * Write a message and a newline to stderr before returning a value.
 *
 * @param The message to write to stderr
 * @param The value to return
 * @result Param 2
 */
trace_n :: !msg .a -> .a | toString msg special msg={#Char}; msg=Int

/**
 * Write a message to stderr and return True. This is useful in guards, for
 * example:
 *
 * ```
 * square x
 * | trace_t x = x * x
 * ```
 *
 * @param The message to write to stderr
 * @result True
 */
trace_t :: !msg -> Bool | toString msg special msg={#Char}; msg=Int

/**
 * Write a message and a newline to stderr and return True. This is useful in
 * guards, for example:
 *
 * ```
 * square x
 * | trace_tn x = x * x
 * ```
 *
 * @param The message to write to stderr
 * @result True
 */
trace_tn :: !msg -> Bool | toString msg special msg={#Char}; msg=Int
