definition module StdInt

/**
 * Class instances and basic functions for the Int type.
 */

// ****************************************************************************************
//	Concurrent Clean Standard Library Module Version 2.0
//	Copyright 1998 University of Nijmegen
// ****************************************************************************************

import	StdOverloaded

instance +				Int		:: !Int !Int -> Int					:== code { addI }

instance -  			Int		:: !Int !Int -> Int					:== code { subI }

instance zero			Int		:: Int								:== code { pushI 0 }

instance *  			Int		:: !Int !Int -> Int					:== code { mulI }

instance /				Int		:: !Int !Int -> Int					:== code { divI }
instance one			Int		:: Int								:== code { pushI 1 }

instance ^				Int
instance abs			Int
instance sign			Int
instance ~				Int		:: !Int -> Int						:== code { negI }

instance ==				Int		:: !Int !Int -> Bool				:== code { eqI }
instance <  			Int		:: !Int !Int -> Bool				:== code { ltI }
instance isEven 		Int		:: !Int -> Bool						:== code { pushI 1 ; and% ; pushI 0 ; eqI }
						// True if arg1 is an even number
instance isOdd			Int		:: !Int -> Bool						:== code { pushI 1 ; and% ; pushI 0 ; eqI ; notB }
						// True if arg1 is an odd  number

instance toInt			Char	:: !Char -> Int						:== code { CtoI }
instance toInt			Int		:: !Int -> Int						:== code { no_op }
instance toInt			Real	:: !Real -> Int						:== code { RtoI }
instance toInt			{#Char}

instance fromInt		Int		:: !Int -> Int						:== code { no_op }
instance fromInt		Char	:: !Int -> Char						:== code { ItoC }
instance fromInt		Real	:: !Int -> Real						:== code { ItoR }
instance fromInt		{#Char}	:: !Int -> {#Char}					:== code { .d 0 1 i ; jsr ItoAC ; .o 1 0 }

// Additional functions for integer arithmetic: 

instance rem Int		:: !Int !Int -> Int							:== code { remI }
				 	//	remainder after integer division
instance gcd Int	//	Greatest common divider
instance lcm Int	//	Least common multiple

//	Operators on Bits:

/**
 * Bitwise disjunction.
 *
 * @param The first integer
 * @param The second integer
 * @result An integer with exactly those bits set that at least one of the parameters has set
 */
(bitor)	infixl  6	:: !Int !Int 	->	Int							:== code { or% }

/**
 * Bitwise conjunction.
 *
 * @param The first integer
 * @param The second integer
 * @result An integer with exactly those bits set that both of the parameters have set
 */
(bitand) infixl 6	:: !Int !Int 	->	Int							:== code { and% }

/**
 * Bitwise exclusive disjunction.
 *
 * @param The first integer
 * @param The second integer
 * @result An integer with exactly those bits set that exactly one of the parameters has set
 */
(bitxor) infixl 6	:: !Int !Int 	->	Int							:== code { xor% }

/**
 * Shift an integer to the left.
 *
 * @param The integer to shift
 * @param The number of places to shift
 * @result The result of the bitshift
 */
(<<)	infix  7	:: !Int !Int 	->	Int							:== code { shiftl% }

/**
 * Shift an integer to the right.
 *
 * @param The integer to shift
 * @param The number of places to shift
 * @result The result of the bitshift
 */
(>>)	infix  7	:: !Int !Int 	->	Int							:== code { shiftr% }

/**
 * Bitwise logical negation.
 *
 * @param An integer
 * @param The one's complement of the parameter
 */
bitnot				:: !Int 		->	Int							:== code { not% }

/**
 * Compile-time macro to check for the integer length of the system.
 *
 * @type .a .a -> .a
 * @param The value if the platform is 64-bit
 * @param The value if the platform is 32-bit
 * @result Either of the parameters, depending on the platform
 */
IF_INT_64_OR_32 int64 int32 :== int32;
