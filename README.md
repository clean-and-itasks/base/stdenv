# stdenv

This is the repository of the [Clean][] StdEnv library. This library contains
many high-frequency functions.

This is a fork of the [upstream][] version and is used to publish a package to
https://clean-lang.org. This version also includes documentation. Periodically
changes from upstream are released in a new version here. The fork matches the
upstream in API and behaviour, and only differs in documentation and things
like CI configuration.

StdEnv is released in the `base-stdenv` package. This package should normally
not be used directly; instead, you should use `base`.

See the documentation in [base][] if you intend to make a merge request for
this repository.

## Maintainer & license

This fork is maintained by [Camil Staps][]. The upstream is maintained by
John van Groningen.

For license details, see the [LICENSE](/LICENSE) file.

[base]: https://gitlab.com/clean-and-itasks/base/base
[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[upstream]: https://gitlab.science.ru.nl/clean-compiler-and-rts/stdenv
