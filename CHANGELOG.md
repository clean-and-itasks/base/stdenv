# Changelog

#### v2.1.1

- Fix: change system modules to definition modules.

### v2.1.0

- Feature: add support for `{..:}` arrays.

## v2.0.0

- Enhancement: add dedicated `_ARRAY_` descriptors for unboxed arrays of `Int`,
  `Real`, `Bool`, 32-bit `Int`, and 32-bit `Real`, to save space. Add
  `_ARRAY_R_` descriptor for unboxed arrays of records.

## v1.0

First tagged version.
